//
//  MapViewController.swift
//  Layer-Parse-iOS-Swift-Example
//
//  Created by forever on 11/21/15.
//  Copyright © 2015 layer. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "MapView"      
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        initLocationManager();
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        locationManager.stopUpdatingLocation()
    }

    // Location Manager helper stuff
    func initLocationManager() {
        
        locationManager                 = CLLocationManager()
        locationManager.delegate        = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locationManager.stopUpdatingLocation()
        
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: { (placemarks, error) -> Void in
            
            if(error != nil) {
                print("Error : " + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count > 0) {
                let pm = placemarks![0] as CLPlacemark
                self.displayLocationInfo(pm)
            } else {
                print( "Error With data")
            }
            
        })
    }
    
    func displayLocationInfo(placemark:CLPlacemark) {
        locationManager.stopUpdatingLocation()
        
        let locationInfo    = placemark.location
        let location        = locationInfo!.coordinate
        
        let span            = MKCoordinateSpanMake(0.05, 0.05)
        let region          = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        //3
        let annotation      = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title    = placemark.administrativeArea
        annotation.subtitle = placemark.country
        mapView.addAnnotation(annotation)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
